import React from 'react'
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';


export default function Tostify() {
    return (
        <>
            <ToastContainer
                position="bottom-right"
                autoClose={5000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
                theme='colored'
            />

            {/* 
                //////Document//////
                    -Add Below Codes in your Project:
                        toast['success']('Blog Inserted Successfully !!')

                    -And This One to Your Return:
                        <Tostify />
            */}
            
        </>
    )
}
