import React, { Component } from "react";
import { Route, Routes } from "react-router-dom";
import { useScrollToTop } from "./CustomHooks/useScrollToTop";
import Error404 from "./Pages/Error404/";
import IntroPage from "./Pages/Website/IntroPage/IntroPage";


function App() {
  useScrollToTop();
  return (
    <>
      <Routes>
        <Route path="/" exact element={<IntroPage />} />
        {/* <Route path="/custom-product/:name" element={<CustomProduct />} /> */}
        {/* 404 Rout */}
        <Route path="/*" element={<Error404 />} />
      </Routes>
    </>
  );
}

export default App;
