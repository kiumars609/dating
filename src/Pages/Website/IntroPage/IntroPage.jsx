import React from "react";
import "./scss/IntroPage.css";
import RightSide from "./RightSide";
import LeftSide from "./LeftSide/LeftSide";

export default function IntroPage() {
  return (
    <>
      <div className="col-md-12 container-fluid vh-100 d-flex p-0">
        <LeftSide />
        <RightSide />
      </div>
    </>
  );
}
