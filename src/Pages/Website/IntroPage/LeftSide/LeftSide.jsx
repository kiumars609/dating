import React from "react";

export default function LeftSide() {
  return (
    <>
      <div className="col-md-4 left-side">
        {/* Start Logo Section */}
        <div className="col-md-12 left-side-logo">
          <img
            src="/assets/images/icon/logo.png"
            className="col-md-2 logo"
            alt="logo"
          />
          <h5 className="title">
            <span>Match</span>Maker
          </h5>
        </div>
        {/* End Logo Section */}
        {/* Start Form Section */}
        <div className="col-md-12 left-side-form">
          <h1>Are you ready for your adventure?</h1>
          <span>Every 14 minutes, someone finds love on MatchMaker</span>
        </div>
        {/* End Form Section */}
      </div>
    </>
  );
}
